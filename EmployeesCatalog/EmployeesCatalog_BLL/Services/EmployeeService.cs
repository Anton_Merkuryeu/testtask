﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesCatalog_BLL.Infrastructure;
using EmployeesCatalog_BLL.Interfaces;
using EmployeesCatalog_BLL.Model;
using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_BLL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IRepositoryContainer container;

        public EmployeeService(IRepositoryContainer container)
        {
            this.container = container;
        }
        public async Task<int> CreateAsync(EmployeeDto entity)
        {
            var employee = new Employee
            {
                Name = entity.Name,
                Position = entity.Position,
                Surname = entity.Surname,
                Id = entity.Id
            };
            return await container.Employee.CreateAsync(employee);
        }

        public async Task<IEnumerable<EmployeeDto>> GetEmployeesAsync()
        {
            var employees = await container.Employee.GetEmployeesAsync();
            var temp = employees.ToList();
            return employees.ToList().Select(emp =>
                new EmployeeDto {Id = emp.Id, Name = emp.Name, Position = emp.Position, Surname = emp.Surname});
        }

        public async Task<EmployeeDto> GetEmployeeByIdAsync(int id)
        {
            var employee = await container.Employee.GetEmployeeByIdAsync(id);
            if (employee == null)
            {
                throw new ValidationException("There aren't employees with this id at the moment");
            }
            var result = new EmployeeDto
            {
                Id = employee.Id,
                Name = employee.Name,
                Position = employee.Position,
                Surname = employee.Surname
            };
            return result;
        }

        public async Task UpdateAsync(EmployeeDto employee)
        {
            var employeeTemp = await container.Employee.GetEmployeeByIdAsync(employee.Id);
            //if layout isn't exist in database, create new layout
            if (employeeTemp == null)
            {
                await CreateAsync(employee);
            }
            else
            {
                employeeTemp.Name = employee.Name;
                employeeTemp.Position = employee.Position;
                await container.Employee.UpdateAsync(employeeTemp);
            }
        }

        public async Task DeleteAsync(int id)
        {
            if (await container.Employee.GetEmployeeByIdAsync(id) == null)
            {
                throw new ValidationException("There aren't employees with this id at the moment");
            }
            await container.Employee.DeleteAsync(id);
        }
    }
}
