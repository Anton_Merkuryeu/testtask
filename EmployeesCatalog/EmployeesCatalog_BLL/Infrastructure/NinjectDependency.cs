﻿using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Repositories;
using Ninject.Modules;

namespace EmployeesCatalog_BLL.Infrastructure
{
    public class NinjectDependency : NinjectModule
    {
        private readonly string connectionString;

        public NinjectDependency(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public override void Load()
        {
            Bind<IRepositoryContainer>().To<RepositoryContainer>()
                .WithConstructorArgument("connectionString", connectionString);
        }
    }
}
