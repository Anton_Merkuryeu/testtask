﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeesCatalog_BLL.Model;

namespace EmployeesCatalog_BLL.Interfaces
{
    public interface IEmployeeService
    {
        Task<int> CreateAsync(EmployeeDto employee);
        Task<IEnumerable<EmployeeDto>> GetEmployeesAsync();
        Task<EmployeeDto> GetEmployeeByIdAsync(int id);
        Task UpdateAsync(EmployeeDto employee);
        Task DeleteAsync(int id);
    }
}
