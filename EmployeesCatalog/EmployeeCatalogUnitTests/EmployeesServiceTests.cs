﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeCatalogUnitTests.FakeRepository;
using EmployeesCatalog_BLL.Infrastructure;
using EmployeesCatalog_BLL.Model;
using EmployeesCatalog_BLL.Services;
using EmployeesCatalog_DAL.Interfaces;
using NUnit.Framework;

namespace EmployeeCatalogUnitTests
{
    [TestFixture]
    class EmployeesServiceTests
    {
        private EmployeeService service;
        private IRepositoryContainer container;

        [SetUp]
        public void InitialData()
        {
            container = new FakeEmployeeContainer();
            service = new EmployeeService(container);
        }

        [Test]
        public async Task CreateEmployee_EmployeeDtoInstance_IdEqual_1()
        {
            var employee = new EmployeeDto {Name = "test", Position = "test", Surname = "test"};
            int id = await service.CreateAsync(employee);
            int expectedId = 1;

            Assert.AreEqual(expectedId,id);
        }

        [Test]
        public async Task GetEntities_WithoutArguments_EmployeesCollection()
        {

            var employeeFirst = new EmployeeDto { Name = "test", Position = "test", Surname = "test" };
            var employeeSecond = new EmployeeDto { Name = "test", Position = "test", Surname = "test" };

            await service.CreateAsync(employeeFirst);
            await service.CreateAsync(employeeSecond);
            var collection = await service.GetEmployeesAsync();
            int expectedCount = 2;

            Assert.AreEqual(expectedCount, collection.Count());
        }

        [Test]
        public async Task GetEntity_WrongId_ValidationException()
        {
            var employee = new EmployeeDto { Name = "test", Position = "test", Surname = "test" };

            await service.CreateAsync(employee);

            Assert.ThrowsAsync<ValidationException>(async () => await service.GetEmployeeByIdAsync(2), "There aren't venues with this id at the moment");
        }

        [Test]
        public async Task GetEntity_CorrectId_GetEntity()
        {
            var employee = new EmployeeDto {Name = "test", Position = "test", Surname = "test", Id = 1};

            await service.CreateAsync(employee);
            var element = await service.GetEmployeeByIdAsync(1);
            var expectedElementsId = 1;

            Assert.AreEqual(expectedElementsId, element.Id);
        }
        [Test]
        public async Task Delete_WrongId_ValidationException()
        {
            var employee = new EmployeeDto { Name = "test", Position = "test", Surname = "test", Id = 1 };

            await service.CreateAsync(employee);

            Assert.ThrowsAsync<ValidationException>(async () => await service.DeleteAsync(2), "There aren't venues with this id at the moment");
        }

        [Test]
        public async Task Delete_CorrectId_EmptyList()
        {
            var employee = new EmployeeDto { Name = "test", Position = "test", Surname = "test", Id = 1 };

            await service.CreateAsync(employee);
            await service.DeleteAsync(1);
            var expected = 0;

            Assert.AreEqual(expected, (await service.GetEmployeesAsync()).Count());
        }

        [Test]
        public async Task Update_VenueBLLinstance_ChangeObject()
        {
            var employeeFirst = new EmployeeDto {Name = "test", Position = "test", Surname = "test", Id = 1};
            var employeeSecond = new EmployeeDto {Name = "test", Position = "test2", Surname = "test", Id = 1};

            await service.CreateAsync(employeeFirst);
            await service.UpdateAsync(employeeSecond);
            var expectedPosition = "test2";

            Assert.AreEqual(expectedPosition, (await service.GetEmployeesAsync()).ToList().Find(item => item.Id == 1).Position);
        }

    }
}
