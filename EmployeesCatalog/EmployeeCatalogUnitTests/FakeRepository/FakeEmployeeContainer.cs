﻿using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Model;

namespace EmployeeCatalogUnitTests.FakeRepository
{
    class FakeEmployeeContainer :IRepositoryContainer
    {
        private FakeEmployeeRepository employee;
        public IRepository<Employee> Employee
        {
            get
            {
                if (employee == null)
                {
                    employee = new FakeEmployeeRepository();
                }
                return employee;
            }
        }
    }
}
