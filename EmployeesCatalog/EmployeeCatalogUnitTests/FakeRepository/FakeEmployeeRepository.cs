﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Model;

namespace EmployeeCatalogUnitTests.FakeRepository
{
    class FakeEmployeeRepository : IRepository<Employee>
    {
        public List<Employee> Container { get; }

        public FakeEmployeeRepository()
        {
            Container=new List<Employee>();
        }
        public Task<int> CreateAsync(Employee employee)
        {
            Container.Add(employee);
            return Task.FromResult(Container.Count);
        }

        public Task<IQueryable<Employee>> GetEmployeesAsync()
        {
            return Task.FromResult(Container.AsQueryable());
        }

        public Task<Employee> GetEmployeeByIdAsync(int id)
        {
            return Task.FromResult(Container.FirstOrDefault<Employee>(item => item.Id == id));
        }

        public async Task UpdateAsync(Employee employee)
        {
            var employeeTemp = Container.FirstOrDefault(item => item.Id == employee.Id);
            //if layout isn't exist in database, create new layout
            if (employeeTemp == null)
                await CreateAsync(employee);
            else
            {
                employeeTemp.Name = employee.Name;
                employeeTemp.Position = employee.Position;
                employeeTemp.Surname = employee.Surname;
                Container.RemoveAt(employee.Id - 1);
                Container.Insert(employee.Id - 1, employeeTemp);
            }
        }

        public async Task DeleteAsync(int id)
        {
            Container.RemoveAt(id - 1);
        }
    }
}
