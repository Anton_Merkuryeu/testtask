﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EmployeesCatalog_BLL.Infrastructure;
using EmployeesCatalog_BLL.Interfaces;
using EmployeesCatalog_BLL.Model;
using EmployeesCatalog_Web.Models;

namespace EmployeesCatalog_Web.Controllers
{
    public class HomeController : Controller
    {
        private IEmployeeService service;

        public HomeController(IEmployeeService service)
        {
            this.service = service;
        }

        public async Task<ActionResult> Index()
        {
            var employees = await service.GetEmployeesAsync();
            var result = EmployeesFromDto(employees);
            return View(result);
        }

        public async Task<ActionResult> Create()
        {
            return View("Edit", new EmployeeViewModel());
        }

        public async Task<ActionResult> Edit(int id)
        {
            var item = await service.GetEmployeeByIdAsync(id);
            var result = EmployeeFromDto(item);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                await service.UpdateAsync(DtoFromEmployee(model));
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            await service.DeleteAsync(id);
            return RedirectToAction("Index");
        }

        private IEnumerable<EmployeeViewModel> EmployeesFromDto(IEnumerable<EmployeeDto> entities)
        {
            var employees=new List<EmployeeViewModel>();
            foreach (var item in entities)
            {
                employees.Add(EmployeeFromDto(item));
            }
            return employees;
        }

        private EmployeeViewModel EmployeeFromDto(EmployeeDto entity)
        {
            return new EmployeeViewModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Position = entity.Position,
                Surname = entity.Surname
            };
        }

        private EmployeeDto DtoFromEmployee(EmployeeViewModel entity)
        {
            return new EmployeeDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Position = entity.Position,
                Surname = entity.Surname
            };
        }
    }
}