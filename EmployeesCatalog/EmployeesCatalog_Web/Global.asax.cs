﻿using System.Web.Mvc;
using System.Web.Routing;
using EmployeesCatalog_BLL.Infrastructure;
using EmployeesCatalog_Web.Infrastructure;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;

namespace EmployeesCatalog_Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            NinjectModule serviceModule=new WebNinjectDependency();
            NinjectModule repositoyModule=new NinjectDependency("EmployeeDatabase");
            var kernel = new StandardKernel(serviceModule, repositoyModule);
            kernel.Unbind<ModelValidatorProvider>();
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
