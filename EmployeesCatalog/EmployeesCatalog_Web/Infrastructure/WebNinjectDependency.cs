﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeesCatalog_BLL.Interfaces;
using EmployeesCatalog_BLL.Services;
using Ninject.Modules;

namespace EmployeesCatalog_Web.Infrastructure
{
    public class WebNinjectDependency : NinjectModule
    {
        public override void Load()
        {
            Bind<IEmployeeService>().To<EmployeeService>();
        }
    }
}