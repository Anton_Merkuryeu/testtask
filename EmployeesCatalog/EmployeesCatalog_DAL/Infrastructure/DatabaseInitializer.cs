﻿using System.Collections.Generic;
using System.Data.Entity;
using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_DAL.Infrastructure
{
    public class DatabaseInitializer : DropCreateDatabaseAlways<EFContext>
    {
        protected override void Seed(EFContext context)
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee {Name = "Liam", Surname = "Nelson", Position = "Lead"},
                new Employee {Name = "Sam", Surname = "White", Position = "Senior"},
                new Employee {Name = "Tim", Surname = "Roth", Position = "Middle"},
                new Employee {Name = "Andrew", Surname = "Johnson", Position = "Junior"},
                new Employee {Name = "John", Surname = "Smith", Position = "QA"}
            };
            context.Employees.AddRange(employees);
            context.SaveChanges();
        }
    }
}
