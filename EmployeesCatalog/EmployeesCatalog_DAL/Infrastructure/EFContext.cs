﻿using System.Data.Entity;
using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_DAL.Infrastructure
{
    public class EFContext : DbContext
    {
        static EFContext()
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
        public EFContext(string connectionString):base(connectionString)
        {
            
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
