﻿using EmployeesCatalog_DAL.Infrastructure;
using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_DAL.Repositories
{
    public class RepositoryContainer : IRepositoryContainer
    {
        private EFContext context;
        private EmployeeRepository employee;

        public RepositoryContainer(string connectionString)
        {
            context=new EFContext(connectionString);
        }

        public IRepository<Employee> Employee
        {
            get
            {
                if (employee == null)
                {
                    employee=new EmployeeRepository(context);
                }
                return employee;
            }
        }
    }
}
