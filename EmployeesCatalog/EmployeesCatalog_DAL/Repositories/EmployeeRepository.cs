﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EmployeesCatalog_DAL.Infrastructure;
using EmployeesCatalog_DAL.Interfaces;
using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_DAL.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private EFContext context;

        public EmployeeRepository(EFContext context)
        {
            this.context=context;
        }
        public async Task<int> CreateAsync(Employee employee)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.Employees.Add(employee);
                    await context.SaveChangesAsync();
                    transaction.Commit();
                    return context.Employees.Last().Id;
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw new Exception(exception.Message);
                }
            }
        }

        public Task<IQueryable<Employee>> GetEmployeesAsync()
        {
            return Task.FromResult<IQueryable<Employee>>(context.Employees);
        }

        public async Task<Employee> GetEmployeeByIdAsync(int id)
        {
            return await context.Employees.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task UpdateAsync(Employee employee)
        {
            using (var transaction=context.Database.BeginTransaction())
            {
                try
                {
                    context.Entry(employee).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw new Exception(exception.Message);
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var employee = await context.Employees.FindAsync(id);
                    context.Entry(employee).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw new Exception(exception.Message);
                }
            }
        }
    }
}
