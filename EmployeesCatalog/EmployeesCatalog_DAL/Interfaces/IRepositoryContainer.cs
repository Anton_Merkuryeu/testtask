﻿using EmployeesCatalog_DAL.Model;

namespace EmployeesCatalog_DAL.Interfaces
{
    public interface IRepositoryContainer
    {
        IRepository<Employee> Employee { get; }
    }
}
