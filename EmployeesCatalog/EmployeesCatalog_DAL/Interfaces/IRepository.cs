﻿using System.Linq;
using System.Threading.Tasks;

namespace EmployeesCatalog_DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<int> CreateAsync(T employee);
        Task<IQueryable<T>> GetEmployeesAsync();
        Task<T> GetEmployeeByIdAsync(int id);
        Task UpdateAsync(T employee);
        Task DeleteAsync(int id);
    }
}
